from flask import Flask, request, render_template, make_response, redirect, url_for
import db
import models as m
import utils as u
import config as c
import time

app = Flask(__name__)


# сделать из этой функции декоратор
def auth(request):
    if c.SESSION_KEY in request.cookies:
        return not (request.cookies[c.SESSION_KEY] == None or db.search_session(request.cookies[c.SESSION_KEY]) == '')
    return False
# def auth(request): @@@@@зачем тут декоратор?@@@@@@@@
#     return not (request.cookies[c.SESSION_KEY] == None or db.search_session(request.cookies[c.SESSION_KEY]) == '')


@app.route('/')
def index():
    return render_template("index.html")


@app.route('/relogin111', methods=['GET', 'POST'])
def relogin111():
    if request.method == "GET":
        if auth(request):
            user_info = db.get_user_by_login(db.get_login_by_session(request.cookies[c.SESSION_KEY]))
            print(user_info.last_name)
            return render_template('relogin111.html', user_info=user_info)
        else:
            return render_template("index.111.html")


# @app.route('/profile', methods=['GET', 'POST'])
# def profile():
# if request.method == "GET":
# return render_template("profile.html")
# elif request.method == "POST":
# if request.cookies[c.SESSION_KEY] == None or db.search_session(request.cookies[c.SESSION_KEY]) == False:
#     # return render_template("profile.html")
#     return None
# else:
#     return db.get_user_by_login(db.get_login_by_session(request.cookies[c.SESSION_KEY]))
# return None


# @app.route('/cookies')
# def index():
#     resp = make_response(render_template("index.html"))
#     resp.set_cookie('session', '123')
#     return resp


@app.route('/register', methods=['GET', 'POST'])
def register():
    if request.method == "GET":
        return render_template("register.html")
    elif request.method == "POST":
        user = m.User(request.form["first_name"], request.form["last_name"], request.form["login"],
                      u.crypto(request.form["password"]))
        if db.is_user_exist(user.login):
            return "user already exist"
        db.save_user(user)
        return 'user created'


@app.route('/login', methods=['GET', 'POST'])
def login():
    login_succsess = 'success'
    login_failed = 'failed'
    login_status = ''
    if request.method == 'POST':
        client = m.OldUser(request.form["login"], u.crypto(request.form["password"]))
        auth = db.authorization(client.login, client.password)
        if auth:
            user_session = u.generate_session()
            login_status = login_succsess
            resp = make_response(render_template("login.html", status=login_status))
            resp.set_cookie(c.SESSION_KEY, user_session)
            db.save_session(client.login, user_session)
            return resp
        else:
            login_status = login_failed
            return render_template("login.html", status=login_status)
    else:
        return render_template('login.html', status=login_status)


@app.route('/create_character', methods=['GET', 'POST'])
def create_character():
    if request.method == 'GET':

        return render_template('create_character.html')

    elif request.method == 'POST':
        if not auth(request):
            return redirect(url_for('login'))
        character = m.Character(request.form["name"], request.form["strength"], request.form["agility"], request.form["intellect"],
                                request.form["race"], request.form["vocation"], time.time())
        user_login = db.get_login_by_session(request.cookies[c.SESSION_KEY])
        db.save_character(character)
        db.map_character(user_login, character.id)
        return render_template('create_character.html')

@app.route('/characters', methods=['GET', 'POST'])
def characters():
    characters = []
    if request.method == 'GET':
        if not auth(request):
            return redirect(url_for('login'))
        user_login = db.get_login_by_session(request.cookies[c.SESSION_KEY])
        character_id = db.get_characters_by_login(user_login)
        print(character_id)
        for id in character_id:
           characters.append(db.get_character_by_id(id))
        print(characters)
        return "ok"


# @app.route('/user/<int:user_id>')
# def show_user(user_id):
#     return f'Profile {users[user_id]["username"]},' \
#            f'{users[user_id]["lastname"]},' \
#            f'{users[user_id]["age"]}'
