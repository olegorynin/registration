class User:
    def __init__(self, first_name, last_name, login, password):
        self.first_name = first_name
        self.last_name = last_name
        self.login = login
        self.password = password


class OldUser:
    def __init__(self, login, password):
        self.login = login
        self.password = password


class UserInfo:
    def __init__(self, first_name, last_name):
        self.first_name = first_name
        self.last_name = last_name

class Character:
    def __init__(self, name, strength, agility, intellect, race, vocation, id):
        self.name = name
        self.strength = strength
        self.agility = agility
        self.intellect = intellect
        self.race = race
        self.vocation = vocation
        self.id = id