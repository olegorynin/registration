import psycopg2
import config as c
import models as m

conn = psycopg2.connect(dbname=c.DBNAME, user=c.USER,
                        password=c.PASSWORD, host=c.HOST, port=c.PORT)
conn.autocommit = True


def save_user(user):
    with conn.cursor() as cursor:
        cursor.execute(
            f'INSERT INTO profile VALUES (\'{user.first_name}\', \'{user.last_name}\',\'{user.login}\',\'{user.password}\')')

def save_character(character):
    with conn.cursor() as cursor:
        cursor.execute(
            f'INSERT INTO characters VALUES (\'{character.name}\',\'{character.strength}\', \'{character.agility}\',\'{character.intellect}\',\'{character.race}\',\'{character.vocation}\',\'{character.id}\')')


def get_user_by_login(login):
    with conn.cursor() as cursor:
        command = f'SELECT * FROM profile WHERE login = \'{login}\';'
        cursor.execute(command)
        for user in cursor:
            return m.UserInfo(user[0], user[1])

def search_session(session):
    with conn.cursor() as cursor:
        command = f'SELECT * FROM sessionuser WHERE session = \'{session}\';'
        cursor.execute(command)
        for session in cursor:
            return True
        return False

def get_login_by_session(session):
    with conn.cursor() as cursor:
        command = f'SELECT * FROM sessionuser WHERE session = \'{session}\';'
        cursor.execute(command)
        for login in cursor:
            return login[1]
        return ''

def is_user_exist(login):
    with conn.cursor() as cursor:
        command = f'SELECT * FROM profile WHERE login = \'{login}\';'
        cursor.execute(command)
        for user in cursor:
            return True
        return False


def authorization(login, password):
    with conn.cursor() as cursor:
        command = f'SELECT * FROM profile WHERE login = \'{login}\' and pasword = \'{password}\';'
        # ttt = f'SELECT * FROM profile WHERE pasword = \'{password}\';'
        # cursor.execute(ttt)
        cursor.execute(command)
        for client in cursor:
            return True
        return False

def save_session(login, cookie):
    with conn.cursor() as cursor:
        cursor.execute(
            f'INSERT INTO sessionuser VALUES (\'{cookie}\', \'{login}\')')

def map_character(login, id):
    with conn.cursor() as cursor:
        cursor.execute(
            f'INSERT INTO charactersusers VALUES (\'{id}\', \'{login}\')')


def get_characters_by_login(login):
    list_of_characters_id = []
    with conn.cursor() as cursor:
        command = f'SELECT * FROM charactersusers WHERE login = \'{login}\';'
        cursor.execute(command)
        for character in cursor:
            list_of_characters_id.append(character[0])
        return list_of_characters_id

def get_character_by_id(id):
    with conn.cursor() as cursor:
        command = f'SELECT * FROM characters WHERE id = \'{id}\';'
        cursor.execute(command)
        for character in cursor:
            return character
        return ''

if __name__ == '__main__':
    get_user_by_login('trewq')
    # search_session('io')
    # if search_session('00') == False:
    #     print("YEa")
    # get_login_by_session('1a06bc574d57397d2a635cc353a856e626ef943a94cf2e1600a348a070d84397')
